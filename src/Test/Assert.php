<?php

$exports["assert'"] = function ($message) {
  return function ($success) use (&$message) {
    return function () use (&$message, &$success) {
      if (!$success) throw new \Exception($message);
      return [];
    };
  };
};

$exports['checkThrows'] = function ($fn) {
  return function () use (&$fn) {
    try {
      $fn();
      return false;
    } catch (Exception $e) {
      return true;
    }
  };
};
